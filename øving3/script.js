class Boble {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.r = Math.random() * 40 + 10;
    this.farge = "rgb(" + Math.floor(Math.random() * 256) + ", " + Math.floor(Math.random() * 256) + ", " + Math.floor(Math.random() * 256) + ")";
    this.egentligFarge = this.farge;
  }

  flytt() {
    this.x += Math.floor(Math.random() * 3 - 1);
    this.y += Math.floor(Math.random() * 3 - 1);
  }
  vis() {
    kontekst.beginPath();
    kontekst.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
    kontekst.fillStyle = this.farge;
    kontekst.strokeStyle = "darkgrey";
    kontekst.stroke();
    kontekst.fill();
  }
  ani(x, y) {
    let a = this.x - x;
    let b = this.y - y
    let avstand = Math.sqrt((a * a + b * b));

    console.log(avstand, this.r);
    if (avstand < this.r) {
      this.farge = "black";
      return true;
    } else {
      this.farge = this.egentligFarge;
      return false;
    }
  }
  holdInnenfor() {
    if (this.x + this.r >= kanvas.width) this.x = kanvas.width - this.r;
    if (this.x - this.r <= 0) this.x = this.r;
    if (this.y - this.r <= 0) this.y = this.r;
    if (this.y + this.r >= kanvas.height) this.y = kanvas.height - this.r;
  }
}

let kanvas = document.getElementById("kanvas");
let kontekst = kanvas.getContext("2d");
let bobler = [];
let dTrykk = false;

function klikk(e) {
  let slettetnoe = false;
  for (let i = 0; i < bobler.length; i++) {
    if (bobler[i].ani(e.x, e.y)) {
      bobler.splice(i, 1);
      slettetnoe = true;
    }
  }
  if (!slettetnoe) bobler.push(new Boble(e.x, e.y));
}

function reset() {
  kontekst.fillStyle = "lightgrey";
  kontekst.fillRect(0, 0, kanvas.width, kanvas.height);
}

// for (var i = 0; i < 10; i++) {
//   bobler.push(new Boble(Math.random()*kanvas.width, Math.random()*kanvas.height, Math.random()*20+10));
// }

function nyBoble() {
  bobler.push(new Boble(Math.random() * kanvas.width, Math.random() * kanvas.height));
}

function tegn() {
  reset();

  for (let i = 0; i < bobler.length; i++) {
    bobler[i].holdInnenfor();
    bobler[i].vis();
    bobler[i].flytt();
  }
}

setInterval(tegn, 100);
setInterval(nyBoble, 1000);

document.addEventListener("click", klikk);
document.addEventListener("mousemove", musbeveg);

function musbeveg(e) {
  for (let i = 0; i < bobler.length; i++) {
    bobler[i].ani(e.x, e.y);
  }
}
