class Konto {
  constructor(kundenr, navn, saldo) {
    this.kundenr = kundenr;
    this.navn = navn;
    this.saldo = saldo;
  }

  innskudd(beløp) {
    this.saldo += beløp;
  }

  uttak(beløp) {
    if (beløp > this.saldo) {
      alert("ikke nok penger på konto");
      return false;
    }
    this.saldo -= beløp;
  }

  kontoInformasjon() {
    return this.navn + " med kundenummer " + this.kundenr + " har " + this.saldo + " kroner på konto";
  }
}

class Barnekonto extends Konto {
  constructor(kundenr, navn) {
    super();
    this.kundenr = kundenr;
    this.navn = navn;
    this.saldo = 200;
  }
}

let tidspunkt = "00:00";
let lise, kari, petter;


function lagLise() {
  if (document.getElementById("lisebarn").checked) lise = new Barnekonto(1, "Lise Jensen");
  else {
    let saldo = Number(document.getElementById("lisesaldo").value);
    lise = new Konto(1, "Lise Jensen", saldo);
  }
}

function lagKari() {
  if (document.getElementById("karibarn").checked) kari = new Barnekonto(2, "Kari Hansen");
  else {
    let saldo = Number(document.getElementById("karisaldo").value);
    kari = new Konto(2, "Kari Hansen", saldo);
  }
}

function lagPetter() {
  if (document.getElementById("petterbarn").checked) petter = new Barnekonto(3, "Petter Olsen");
  else {
    let saldo = Number(document.getElementById("pettersaldo").value);
    petter = new Konto(3, "Petter Olsen", saldo);
  }
}


function dag() {
  lagLise();
  lagKari();
  lagPetter();

  visKonti();

  tidspunkt = "10:30";
  kari.uttak(300);
  visKonti();

  tidspunkt = "11:00";
  lise.innskudd(4000);
  visKonti();

  petter.innskudd(3000);
  visKonti();

  tidspunkt = "12:15";
  kari.uttak(250);
  petter.innskudd(250);
  visKonti();

  tidspunkt = "17:30";
  kari.uttak(800);
  visKonti();
}

function visKonti() {
  document.getElementById("konti").innerHTML += "<h2>" + tidspunkt + "</h2>" + lise.kontoInformasjon() + "<br>" + kari.kontoInformasjon() + "<br>" + petter.kontoInformasjon() + "<hr>";
}

// document.addEventListener("click", museklikk, false);
//
// //henter museklikk
// function museklikk(e) {
//   visKonti();
// }
