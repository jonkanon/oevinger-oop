class Bil {
  constructor(regnr, merke, årsmodell, hastighet){
    this.regnr = regnr;
    this.merke = merke;
    this.årsmodell = årsmodell;
    this.hastighet = hastighet;
  }

  gass(){
    this.hastighet += 10;
  }

  brems(){
    this.hastighet -= 10;
  }

}
let volvo, troll, jeep;

function lagBiler(){
  volvo = new Bil(123, "volvo", 2008, 90);
  troll = new Bil(001, "troll", 1956, 30);
  jeep = new Bil(200, "jeep", 1995, 200);
  visbiler();
}

function visbiler(){
  document.getElementById("utdata").innerHTML = volvo.merke + ": " + volvo.hastighet + "<button onclick=\"volvo.gass()\">gass</button>" + "<button onclick=\"volvo.brems()\">brems</button>" +
                                        "<br>"+ jeep.merke + ": " + jeep.hastighet + "<button onclick=\"jeep.gass()\">gass</button>" + "<button onclick=\"jeep.brems()\">brems</button>" +
                                        "<br>"+ troll.merke + ": " + troll.hastighet + "<button onclick=\"troll.gass()\">gass</button>" + "<button onclick=\"troll.brems()\">brems</button>";
}

document.addEventListener("click", museklikk, false);

//henter museklikk
function museklikk(e) {
  visbiler();
}
