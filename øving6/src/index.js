// @flow
import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { connection } from './mysql_connection';
import { studentService } from './services';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

class Meny extends Component {
  render() {
    return (
      <nav>
        <NavLink to="/students"> Studenter </NavLink> <NavLink to="/emner">Emner</NavLink>{' '}
      </nav>
    );
  }
}

class StudentList extends Component {
  students = [];

  render() {
    return (
      <div>
        <br />
        <NavLink to="/students/nystudent"> Legg til student </NavLink>{' '}
        <ul>
          {' '}
          {this.students.map(student => (
            <li key={student.id}>
              <NavLink
                activeStyle={{
                  color: 'darkblue'
                }}
                to={'/students/' + student.id}
              >
                {' '}
                {student.name}{' '}
              </NavLink>{' '}
            </li>
          ))}{' '}
        </ul>{' '}
      </div>
    );
  }

  mounted() {
    connection.query('select id, name from Students', (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.students = results;
    });
  }
}

class StudentDetaljer extends Component<{
  match: {
    params: {
      id: any
    }
  }
}> {
  emner = [];
  studnavn = '';
  studmail = '';

  render() {
    if (this.props.match.params.id == 'nystudent') return null;
    return (
      <div>
        <form>
          Navn:
          <input type="text" onChange={event => (this.studnavn = event.target.value)} value={this.studnavn} /> <br />
          Epost: <input
            type="text"
            onChange={event => (this.studmail = event.target.value)}
            value={this.studmail}
          />{' '}
          <br />
          <button type="button" onClick={this.save}>
            Lagre endringer{' '}
          </button>{' '}
          <button type="button" onClick={this.slett}>
            Slett student{' '}
          </button>{' '}
        </form>{' '}
        <br />
        Påmeldte emner:
        <ul>
          {' '}
          {this.emner.map(emne => (
            <li key={emne.kode}> {emne.kode + ' ' + emne.navn} </li>
          ))}{' '}
        </ul>{' '}
      </div>
    );
  }

  mounted() {
    connection.query(
      'select name, id, email from Students where id=?',
      [this.props.match.params.id],
      (error, results) => {
        if (error) console.error(error);
        this.studnavn = results[0].name;
        this.studmail = results[0].email;
      }
    );

    connection.query(
      'select e.kode, e.navn, s.name from Students s join Students_emner se on s.id=se.stud_id join Emner e on e.id = se.emne_id where s.id =?',
      [this.props.match.params.id],
      (error, results) => {
        if (error) return console.error(error);
        this.emner = results;
      }
    );
  }
  slett() {
    studentService.slettStudent(this.props.match.params.id, () => {
      history.push('/students');
    });
  }
  save() {
    studentService.updateStudent(this.props.match.params.id, this.studnavn, this.studmail, () => {
      history.push('/students');
    });
  }
}

class EmneListe extends Component {
  emner = [];

  render() {
    return (
      <div>
        <br />
        <NavLink to="/emner/nyttemne"> Legg til emne </NavLink>{' '}
        <ul>
          {' '}
          {this.emner.map(emne => (
            <li key={emne.id}>
              <NavLink
                activeStyle={{
                  color: 'darkblue'
                }}
                to={'/emner/' + emne.id}
              >
                {' '}
                {emne.navn}{' '}
              </NavLink>{' '}
            </li>
          ))}{' '}
        </ul>{' '}
      </div>
    );
  }
  mounted() {
    connection.query('select id, navn from Emner', (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return
      this.emner = results;
    });
  }
}

class EmneDetaljer extends Component<{
  match: {
    params: {
      id: any
    }
  }
}> {
  emnekode: string = '';
  emnenavn: string = '';
  id: string = '';
  studenter: Array<Object> = [];

  render() {
    if (this.props.match.params.id == 'nyttemne') return null;
    else
      return (
        <div>
          Detaljer:
          <form>
            Kode: <input type="text" onChange={event => (this.emnekode = event.target.value)} value={this.emnekode} />{' '}
            <br />
            Navn: <input
              type="text"
              onChange={event => (this.emnenavn = event.target.value)}
              value={this.emnenavn}
            />{' '}
            <br />
            <button onClick={this.lagre} type="button">
              Lagre endringer{' '}
            </button>{' '}
            <button onClick={this.slett} type="button">
              Slett emne{' '}
            </button>{' '}
          </form>{' '}
          <br />
          Påmeldte studenter:
          <ul>
            {' '}
            {this.studenter.map(student => (
              <li key={student.id}> {student.name} </li>
            ))}{' '}
          </ul>{' '}
        </div>
      );
  }

  lagre() {
    studentService.updateEmne(this.emnekode, this.emnenavn, this.id, () => {
      history.push('/emner');
    });
  }
  slett() {
    studentService.slettEmne(this.id, () => {
      history.push('/emner');
    });
  }

  mounted() {
    connection.query('select kode, id, navn from Emner where id=?', [this.props.match.params.id], (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return
      this.id = results[0].id;
      this.emnekode = results[0].kode;
      this.emnenavn = results[0].navn;
    });

    connection.query(
      'select s.name, s.id from Students s join Students_emner se on s.id=se.stud_id join Emner e on e.id = se.emne_id where e.id =?',
      [this.props.match.params.id],
      (error, results) => {
        if (error) console.error(error);
        this.studenter = results;
      }
    );
  }
}

class LeggTilStudent extends Component {
  navn = '';
  epost = '';

  render() {
    return (
      <div>
        Ny student:
        <form>
          Navn: <input type="text" onChange={event => (this.navn = event.target.value)} value={this.navn} /> <br />
          Epost: <input type="text" onChange={event => (this.epost = event.target.value)} value={this.epost} /> <br />
          <button onClick={this.lagre} type="button">
            Legg til emne{' '}
          </button>{' '}
        </form>{' '}
      </div>
    );
  }

  lagre() {
    studentService.nyStudent(this.navn, this.epost, () => {
      history.push('/emner');
    });
  }
}

class LeggTilEmne extends Component {
  emnenavn = '';
  emnekode = '';

  render() {
    return (
      <div>
        Nytt emne:
        <form>
          Kode: <input type="text" onChange={event => (this.emnekode = event.target.value)} value={this.emnekode} />{' '}
          <br />
          Navn: <input
            type="text"
            onChange={event => (this.emnenavn = event.target.value)}
            value={this.emnenavn}
          />{' '}
          <br />
          <button onClick={this.lagre} type="button">
            Legg til emne{' '}
          </button>{' '}
        </form>{' '}
      </div>
    );
  }

  lagre() {
    studentService.nyttEmne(this.emnekode, this.emnenavn, () => {
      history.push('/emner');
    });
  }
}

ReactDOM.render(
  <HashRouter>
    <div>
      <Meny />
      <Route path="/students/" component={StudentList} />
      <Route path="/students/:id" component={StudentDetaljer} />
      <Route path="/students/nystudent" component={LeggTilStudent} />
      <Route path="/emner/" component={EmneListe} />
      <Route exact path="/emner/:id" component={EmneDetaljer} />
      <Route path="/emner/nyttemne" component={LeggTilEmne} />
    </div>{' '}
  </HashRouter>,
  document.getElementById('root')
);
