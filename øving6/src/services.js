import { connection } from './mysql_connection';

class StudentService {
  getStudents(success) {
    connection.query('select * from Students', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getStudent(id, success) {
    connection.query('select * from Students where id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results[0]);
    });
  }

  updateEmne(kode, navn, id, success) {
    connection.query('update Emner set kode=?, navn=? where id=?', [kode, navn, id], (error, results) => {
      if (error) return console.log(error);

      success();
    });
  }

  updateStudent(id, name, email, success) {
    connection.query('update Students set name=?, email=? where id=?', [name, email, id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  slettEmne(id, success) {
    connection.query('delete from Emner where id = ?', [id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  slettStudent(id, success) {
    connection.query('delete from Students where id =?', [id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  nyStudent(navn, epost, success) {
    connection.query('insert into Students (name, email) value(?,?)', [navn, epost], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  nyttEmne(kode, navn, success) {
    connection.query('insert into Emner (kode, navn) values(?,?)', [kode, navn], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }
}
export let studentService = new StudentService();
