import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { connection } from './mysql_connection';

class Meny extends Component {
  render() {
    return (
      <nav>
        <NavLink to="/students">Studenter</NavLink> <NavLink to="/emner">Emner</NavLink>
      </nav>
    );
  }
}

class StudentList extends Component {
  students = [];

  render() {
    return (
      <ul>
        {this.students.map(student => (
          <li key={student.id}>
            <NavLink activeStyle={{ color: 'darkblue' }} to={'/students/' + student.id}>
              {student.name}
            </NavLink>
          </li>
        ))}
      </ul>
    );
  }

  mounted() {
    connection.query('select id, name from Students', (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.students = results;
    });
  }
}

class StudentEmner extends Component {
  emner = [];
  navn = [];

  render() {
    return (
      <div>
        {this.navn.map(navn => (
          <p key={navn.id}>Påmelde fag for {navn.name}:</p>
        ))}
        <ul>
          {this.emner.map(emne => (
            <li key={emne.kode}>{emne.kode + ' ' + emne.navn}</li>
          ))}
        </ul>
      </div>
    );
  }

  mounted() {
    connection.query('select name, id from Students where id=?', [this.props.match.params.id], (error, results) => {
      if (error) console.error(error);
      this.navn = results;
    });

    connection.query(
      'select se.kode, e.navn,s.name from Students s join Students_emner se on s.id=se.id join Emner e on e.kode = se.kode where s.id =?',
      [this.props.match.params.id],
      (error, results) => {
        if (error) return console.error(error);
        this.emner = results;
      }
    );
  }
}

class EmneListe extends Component {
  emner = [];

  render() {
    return (
      <ul>
        {this.emner.map(emne => (
          <li key={emne.kode}>
            <NavLink activeStyle={{ color: 'darkblue' }} to={'/emner/' + emne.kode}>
              {emne.navn}
            </NavLink>
          </li>
        ))}
      </ul>
    );
  }
  mounted() {
    connection.query('select kode, navn from Emner', (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return
      this.emner = results;
    });
  }
}

class EmneDetaljer extends Component {
  emne = [];
  studenter = [];

  render() {
    return (
      <div>
        Detaljer:
        {this.emne.map(emne => (
          <div key={emne.kode}>
            Kode: {emne.kode}
            <br />
            Navn: {emne.navn}
          </div>
        ))}
        <br />
        Påmeldte studenter:
        <ul>
          {this.studenter.map(student => (
            <li key={student.id}>{student.name}</li>
          ))}
        </ul>
      </div>
    );
  }

  mounted() {
    connection.query('select kode, navn from Emner where kode=?', [this.props.match.params.kode], (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return
      this.emne = results;
    });

    connection.query(
      'select s.name, s.id from Students s join Students_emner se on s.id=se.id join Emner e on e.kode = se.kode where e.kode =?',
      [this.props.match.params.kode],
      (error, results) => {
        if (error) console.error(error);
        this.studenter = results;
      }
    );
  }
}

// class StudentDetails extends Component {
//   name = '';
//
//   render() {
//     return (
//       <div>
//         <h3>Navn: {this.name}</h3>
//         <p>Påmeldte emner: </p>
//         // <Emneliste />
//       </div>
//     );
//   }
//
//   mounted() {
//     connection.query('select name from Students where id=?', [this.props.match.params.id], (error, results) => {
//       if (error) return console.error(error); // If error, show error in console (in red text) and return
//
//       this.name = results[0].name;
//     });
//   }
// }

ReactDOM.render(
  <HashRouter>
    <div>
      <Meny />
      <Route path="/students/" component={StudentList} />
      <Route path="/students/:id" component={StudentEmner} />
      <Route path="/emner/" component={EmneListe} />
      <Route path="/emner/:kode" component={EmneDetaljer} />
    </div>
  </HashRouter>,
  document.getElementById('root')
);
